/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include <QWidget>
#include <QTextEdit>
#include <QObject>
#include <QPoint>

class QWidget;
class QResizeEvent;
class QPaintEvent;
class QKeyEvent;
class QMouseEvent;
class PieceTable;

class TextEditView : public QWidget 
{
	Q_OBJECT

public:
	TextEditView(PieceTable* pTable, QWidget* parent = 0);
	
protected:
	QSize sizeHint() const;
	void paintEvent(QPaintEvent* event);
	void keyPressEvent(QKeyEvent* event);
	void mousePressEvent(QMouseEvent* event);

private:
	QPoint RepositionCaret(int x, int y);
	bool RemoveChar(int pos);
	
private:
	PieceTable* m_pTable;

	int m_nCursorPos;
};
