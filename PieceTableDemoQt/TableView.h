/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include <QWidget>
#include <QObject>

class QWidget;
class QResizeEvent;
class QPaintEvent;
class PieceTable;

class TableView : public QWidget
{
	Q_OBJECT

public:
	TableView(PieceTable* pTable, QWidget* parent = 0);
	
protected:
	void paintEvent(QPaintEvent* event);

private:
	PieceTable* m_pTable;
};
