/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "GraphicalView.h"
#include "../PieceTable/piecetable.h"
#include <QPainter>
#include <QFontMetrics>
#include <QApplication>
#include <QSize>
#include <QRect>

GraphicalView::GraphicalView(PieceTable* pTable, QWidget* parent/*= 0*/)
	: QWidget(parent)
	, m_pTable(pTable)
{
	setAutoFillBackground(true);
}

void DrawNodeBackground(QPainter* painter, QRect rect, QColor, QColor bgColor)
{
	QBrush brush(bgColor, Qt::SolidPattern);

	Qt::BGMode bgMode = painter->backgroundMode();
	painter->setBackgroundMode(Qt::OpaqueMode);

	painter->fillRect(rect, brush);
	
	painter->setBackgroundMode(bgMode);
}

void DrawNode(QPainter* painter, QRect rect, QString text, QColor fgColor, QColor bgColor, QSize* pTextSize)
{
	QRect bounds = QFontMetrics(painter->font()).boundingRect(text);

	QRect rc(rect.left(), rect.top(), bounds.width(), bounds.height());

	DrawNodeBackground(painter, rc, fgColor, bgColor);

	Qt::BGMode bgMode = painter->backgroundMode();
	painter->setBackgroundMode(Qt::TransparentMode);
	
	QPen fgPen(fgColor);
	painter->setPen(fgPen);
	
	painter->drawText(rect, text);

	painter->setBackgroundMode(bgMode);

	if (pTextSize) {
		pTextSize->setWidth(bounds.width());
		pTextSize->setHeight(bounds.height());
	}
}

void GraphicalView::paintEvent(QPaintEvent*)
{
	QPainter painter(this);

	QFont font("Times", 28);
	painter.setFont(font);
	painter.setRenderHint(QPainter::Antialiasing, true);

	painter.setPen(QPen(Qt::blue, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));

	const int OFFSET = 10;
	const int LEFT   = 10;
	const int TOP    = 10;
	const int HEIGHT = 47;

	QColor origColor(0x7F, 0x44, 0xD6);
	QColor modColor(0xC9, 0x36, 0xD3);
	QColor otherColor(0x4D, 0x54, 0xD8);

	QRect rcStart(LEFT, TOP, 100, HEIGHT);
	PieceTable::PieceTableIterator it = m_pTable->mPieceTable.begin();
	while (it != m_pTable->mPieceTable.end()) {
		PieceTable::piece* p = &(*it);
		QSize sz;
		QString text = QString::fromWCharArray(m_pTable->getBuffer(p) + p->offset, (int)p->length);
		DrawNode(&painter, rcStart, text, origColor, p->type == PieceTable::originalBuffer ? origColor : modColor, &sz);
		rcStart.setLeft(rcStart.left() + sz.width() + OFFSET);
		rcStart.setRight(rcStart.left());
		rcStart.setWidth(100);
		rcStart.setHeight(HEIGHT);
		it++;
	}

	painter.setFont(QApplication::font());

	// Draw original buffer
	QRect rcOrgBuffer(LEFT, rcStart.bottom() + OFFSET, 100, HEIGHT);
	painter.drawText(rcOrgBuffer, QString("Original buffer: "));
	rcOrgBuffer.moveLeft(120);
	QSize sz;
	if (m_pTable->mOriginalBuffer) {
		QString text = QString::fromWCharArray(m_pTable->mOriginalBuffer, wcslen(m_pTable->mOriginalBuffer));
		QRect bounds = QFontMetrics(painter.font()).boundingRect(text);
		rcOrgBuffer.setWidth(bounds.width()+1);
		rcOrgBuffer.setHeight(bounds.height()+1);
		DrawNode(&painter, rcOrgBuffer, text, otherColor, origColor, &sz);
	}

	// Draw modify/append buffer
	QRect rcModBuffer(LEFT, rcOrgBuffer.bottom() + OFFSET, 200, HEIGHT);
	painter.drawText(rcModBuffer, QString("Modify buffer: "));
	rcModBuffer.moveLeft(120);
	if (m_pTable->mAppendBuffer) {
		QString text = QString::fromWCharArray(m_pTable->mAppendBuffer, m_pTable->mAppendBufferLength);
		QRect bounds = QFontMetrics(painter.font()).boundingRect(text);
		rcModBuffer.setWidth(bounds.width()+1);
		rcModBuffer.setHeight(bounds.height()+1);
		DrawNode(&painter, rcModBuffer, text, otherColor, modColor, &sz);
	}
}
