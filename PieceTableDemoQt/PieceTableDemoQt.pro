######################################################################
# Automatically generated by qmake (3.0) Wed Jun 25 20:25:43 2014
######################################################################

TEMPLATE = app
TARGET = PieceTableDemoQt
INCLUDEPATH += .
DESTDIR=bin
OBJECTS_DIR=obj
MOC_DIR=obj
QT += widgets

# Input
HEADERS += CustomPaintEvent.h \
           GraphicalView.h \
           TableView.h \
           TextEditView.h \
           ../PieceTable/piecetable.h
SOURCES += CustomPaintEvent.cpp \
           GraphicalView.cpp \
           main.cpp \
           TableView.cpp \
           TextEditView.cpp \
           ../PieceTable/piecetable.cpp
