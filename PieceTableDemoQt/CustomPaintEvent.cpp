/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */
#include "CustomPaintEvent.h"

CustomPaintEvent::CustomPaintEvent()
	: QEvent(registeredEventType())
{
}

CustomPaintEventFilter::CustomPaintEventFilter(QObject *const parent)
	: QObject(parent)
{
}

bool CustomPaintEventFilter::eventFilter(QObject *obj, QEvent *event)
{
	if(event && (event->type() == CustomPaintEvent::registeredEventType())) {
		emit repaint();
		return true;
	}
	return QObject::eventFilter(obj, event);
}
