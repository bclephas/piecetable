/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include <QApplication>
#include <QVBoxLayout>
#include "CustomPaintEvent.h"
#include "TextEditView.h"
#include "GraphicalView.h"
#include "TableView.h"
#include "../PieceTable/piecetable.h"

int main(int argc, char **args)
{
	QApplication app(argc, args);
	PieceTable table;

	table.insert(0, L"Hello World", 11);
	table.insert(0, L"Hi! ", 4);
	table.insert(10, L"Large ", 6);

	TextEditView textview(&table);
	textview.setGeometry(QRect(0, 0, 400, 200));
	
	GraphicalView graphview(&table);
	TableView tableview(&table);

	QVBoxLayout *layout = new QVBoxLayout;

	CustomPaintEventFilter* const eventFilter = new CustomPaintEventFilter(&app);
	app.installEventFilter(eventFilter);
	
	textview.connect(eventFilter, SIGNAL(repaint()), SLOT(update()));
	graphview.connect(eventFilter, SIGNAL(repaint()), SLOT(update()));
	tableview.connect(eventFilter, SIGNAL(repaint()), SLOT(update()));
	
	layout->setSizeConstraint(QLayout::SetDefaultConstraint);
	layout->addWidget(&textview);
	layout->addWidget(&graphview);
	layout->addWidget(&tableview);

	QWidget *window = new QWidget;
	window->setMinimumSize(640, 480);
	window->setLayout(layout);
	window->show();

	return app.exec();
}

