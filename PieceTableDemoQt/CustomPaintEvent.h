/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */
/* See http://stackoverflow.com/questions/2495254/qt-send-signal-to-main-application-window */

#include <QEvent>
#include <QObject>

class CustomPaintEvent : public QEvent
{
public:
	static Type registeredEventType()
	{
		static Type myType = static_cast<QEvent::Type>(QEvent::registerEventType());
		return myType;
	}

	CustomPaintEvent();
};

class CustomPaintEventFilter : public QObject
{
	Q_OBJECT

public:
	CustomPaintEventFilter(QObject *const parent);

signals:
	void repaint();

protected:
	bool eventFilter(QObject *obj, QEvent *event);
};
