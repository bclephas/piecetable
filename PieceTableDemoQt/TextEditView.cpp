/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "TextEditView.h"
#include "CustomPaintEvent.h"
#include "../PieceTable/piecetable.h"
#include <QApplication>
#include <QPainter>
#include <QKeyEvent>
#include <QDebug>
#include <algorithm>

QFont g_font;

TextEditView::TextEditView(PieceTable* pTable, QWidget* parent/*= 0*/)
	: QWidget(parent)
	, m_pTable(pTable)
	, m_nCursorPos(0)
{
	setFocusPolicy(Qt::StrongFocus);

	QFont font("Times", 28);
	g_font = font;
}

QSize TextEditView::sizeHint() const
{
	QSize hint = parentWidget()->size();
	hint.setHeight(hint.height() / 3);
	return hint;
}

void TextEditView::paintEvent(QPaintEvent* /*e*/)
{
	QPainter painter(this);

	painter.setFont(g_font);
	painter.setRenderHint(QPainter::Antialiasing, true);

	wchar_t buf[4096+1];
	m_pTable->render(0, buf, 4096);
	QString text = QString::fromWCharArray(buf, wcslen(buf));

	Qt::BGMode bgMode = painter.backgroundMode();
	painter.setBackgroundMode(Qt::TransparentMode);

	QPen pen(Qt::black, 2, Qt::SolidLine);

	QRect bounds = QFontMetrics(painter.font()).boundingRect(text);
	
	QPoint pt(0, bounds.height() + 1);
	painter.drawText(pt, text);
	
	painter.setBackgroundMode(bgMode);

	// Paint caret
	QPoint caretPosInPx = RepositionCaret(m_nCursorPos, 0);
	QLine line(caretPosInPx.x(), 0, caretPosInPx.x()+1, bounds.height()+1);
	painter.drawLine(line);
}

QPoint TextEditView::RepositionCaret(int x, int y)
{
	wchar_t buf[4096+1];
	m_pTable->render(0, buf, 4096);
	QString text = QString::fromWCharArray(buf, wcslen(buf));
	QFontMetrics metrics(g_font);

	int totalw = 0;
	for (int i = 0; i < x; i++) {	// SLOOWWW!! But allowed for this demo app
		totalw += metrics.width(text[i]);
	}

	return QPoint(totalw, y);
}

bool TextEditView::RemoveChar(int pos)
{
	m_pTable->remove(pos, 1);

	update();
	QApplication::postEvent(parentWidget(), new CustomPaintEvent());

	return true;
}

void TextEditView::keyPressEvent(QKeyEvent* e)
{
	if (e->text()[0].isPrint()) {
		wchar_t text[1024];
		e->text().toWCharArray(text);
		
		m_pTable->insert(m_nCursorPos, const_cast<wchar_t*>(&text[0]), 1);

		m_nCursorPos = std::min(m_nCursorPos + 1, static_cast<int>(m_pTable->size()));
		RepositionCaret(m_nCursorPos, 0);

		update();
		QApplication::postEvent(parentWidget(), new CustomPaintEvent());
		
		return;
	}

	switch (e->key()) {
		case Qt::Key_Backspace:
			if (m_nCursorPos > 0 && RemoveChar(m_nCursorPos - 1)) {
				m_nCursorPos = std::max(m_nCursorPos - 1, 0);
				RepositionCaret(m_nCursorPos, 0);
			}
			break;

		case Qt::Key_Delete:
			RemoveChar(m_nCursorPos);
			// No need to reposition caret
			break;

		case Qt::Key_Left:
			m_nCursorPos = std::max(m_nCursorPos - 1, 0);
			RepositionCaret(m_nCursorPos, 0);
			break;

		case Qt::Key_Right:
			m_nCursorPos = std::min(m_nCursorPos + 1, static_cast<int>(m_pTable->size()));
			RepositionCaret(m_nCursorPos, 0);
			break;

		case Qt::Key_Home:
			m_nCursorPos = 0;
			RepositionCaret(m_nCursorPos, 0);
			break;

		case Qt::Key_End:
			m_nCursorPos = m_pTable->size();
			RepositionCaret(m_nCursorPos, 0);
			break;
	}

	update();
	QApplication::postEvent(parentWidget(), new CustomPaintEvent());
}

void TextEditView::mousePressEvent(QMouseEvent* /*e*/)
{
	setFocus();
}
