/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "TableView.h"
#include "../PieceTable/piecetable.h"
#include <QPainter>
#include <QFontMetrics>
#include <QApplication>
#include <QSize>
#include <QRect>
#include <QDebug>
#include <algorithm>

TableView::TableView(PieceTable* pTable, QWidget* parent/*= 0*/)
	: QWidget(parent)
	, m_pTable(pTable)
{
	setAutoFillBackground(true);
}

void DrawCell(QPainter* painter, QRect rect, QString text, QColor fgColor, QColor bgColor, QColor borderColor)
{
	QPen pen(fgColor);
	QPen borderPen(borderColor);
	QBrush brush(bgColor, Qt::SolidPattern);

	int penWidth = pen.width() + 1;
	
	QRect bounds = QFontMetrics(painter->font()).boundingRect(text);
	QRect rc(rect.left(), rect.top(), rect.width(), bounds.height()+1+penWidth);
	
	// Draw cell background (including border)
	painter->fillRect(rc, brush);
	painter->setPen(pen);
	painter->drawRect(rc);

	// Draw text
	painter->setPen(pen);

	Qt::BGMode bgMode = painter->backgroundMode();
	painter->setBackgroundMode(Qt::TransparentMode);

	rc.moveLeft(rc.left() + (3*penWidth));
	rc.moveTop(rc.top() + penWidth);
	painter->drawText(rc, text);

	painter->setBackgroundMode(bgMode);
}

void TableView::paintEvent(QPaintEvent*)
{
	QPainter painter(this);

	//QFont font("Times", 28);
	//painter.setFont(font);
	painter.setRenderHint(QPainter::Antialiasing, true);

	QRect rect = painter.window();

	QColor blackColor(  0,   0,   0);
	QColor whiteColor(255, 255, 255);
	QColor DebugColor(128, 128, 128);

	const int LEFT = 10;
	const int TOP = 10;
	const int HEIGHT = 40;
	const int OFFSET_COL_WIDTH = 40;
	const int LENGTH_COL_WIDTH = 40;
	const int TEXT_COL_WIDTH = rect.width() - (2*LEFT) - OFFSET_COL_WIDTH - LENGTH_COL_WIDTH - LEFT;

	QRect rcStart(LEFT, TOP, 10, HEIGHT);

	int tableRow = 0;
	PieceTable::PieceTableIterator it = m_pTable->mPieceTable.begin();
	while (it != m_pTable->mPieceTable.end()) {
		PieceTable::piece* p = &(*it);

		QRect bounds;
		QColor bgColor = whiteColor;
		if (tableRow++ % 2 == 0) bgColor = QColor(195, 195, 195);

		// Draw offset in piece
		QString offsetText = QString::number((int)p->offset);
		bounds = QFontMetrics(painter.font()).boundingRect(offsetText);
		rcStart.setLeft(LEFT);
		rcStart.setWidth(OFFSET_COL_WIDTH);
		rcStart.setHeight(bounds.height()+1);
		DrawCell(&painter, rcStart, offsetText, blackColor, bgColor, blackColor);

		// Draw length in piece
		QString lengthText = QString::number((int)p->length);
		bounds = QFontMetrics(painter.font()).boundingRect(lengthText);
		rcStart.setLeft(rcStart.right());
		rcStart.setWidth(LENGTH_COL_WIDTH);
		rcStart.setHeight(bounds.height()+1);
		DrawCell(&painter, rcStart, lengthText, blackColor, bgColor, blackColor);

		// Draw text
		size_t len = std::min((int)p->length, 1024);
		QString text = QString::fromWCharArray(m_pTable->getBuffer(p) + p->offset, (int)len);
		bounds = QFontMetrics(painter.font()).boundingRect(text);
		rcStart.setLeft(rcStart.right());
		rcStart.setWidth(TEXT_COL_WIDTH);
		rcStart.setHeight(bounds.height()+1);
		DrawCell(&painter, rcStart, text, blackColor, bgColor, blackColor);

		rcStart.setTop(rcStart.top() + bounds.height() + 1);

		it++;
	}
}
