#define _WIN32_WINNT 0x501

#include <windows.h>
#include <tchar.h>

#include "resource.h"
#include "GraphicalView.h"
#include "TableView.h"
#include "TextEditView.h"

#include "../PieceTable/piecetable.h"

HWND hWndMain;
HWND hWndGraphView;
HWND hWndTableView;
HWND hWndTextEditView;

PieceTable g_PieceTable;

TCHAR g_szAppName[] = _T("Piece Table Demo");
TCHAR g_szInitialText[] = L"Hello World";

const int DEFAULT_WIDTH  = 800;
const int DEFAULT_HEIGHT = 600;

LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int width  = LOWORD(lParam);
	int height = HIWORD(lParam);

	int unitHeight = height / 5;
	int textEditHeight = unitHeight;
	int graphHeight = unitHeight;
	int tableHeight = 4 * unitHeight;

	switch(msg)
	{
	case WM_CREATE:
		g_PieceTable.open(g_szInitialText, wcslen(g_szInitialText));
		//g_PieceTable.insert(6, L"good ", 5);
		hWndGraphView = CreateGraphicalView(hWnd, &g_PieceTable);
		if (hWndGraphView == NULL) {
			MessageBox(NULL, L"Window creation failed!", L"Graphical View", MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}
		hWndTableView = CreateTableView(hWnd, &g_PieceTable);
		if (hWndTableView == NULL) {
			MessageBox(NULL, L"Window creation failed!", L"Table View", MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}
		hWndTextEditView = CreateTextEditView(hWnd, &g_PieceTable);
		if (hWndTextEditView == NULL) {
			MessageBox(NULL, L"Window creation failed!", L"Text Edit View", MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}
		return 0;

	case WM_USER:
		InvalidateRect(hWndGraphView, 0, FALSE);
		InvalidateRect(hWndTableView, 0, FALSE);
		return 0;

	case WM_CLOSE:
		DestroyWindow(hWndTextEditView);
		DestroyWindow(hWndTableView);
		DestroyWindow(hWndGraphView);
		DestroyWindow(hWnd);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_SIZE:
		MoveWindow(hWndTextEditView, 0, 0, width, textEditHeight, TRUE);
		MoveWindow(hWndGraphView, 0, textEditHeight, width, graphHeight, TRUE);
		MoveWindow(hWndTableView, 0, textEditHeight+graphHeight, width, tableHeight, TRUE);
		InvalidateRect(hWndTextEditView, 0, 0);
		InvalidateRect(hWndGraphView, 0,0);
		InvalidateRect(hWndTableView, 0,0);
		return 0;

	case WM_SETFOCUS:
		SetFocus(hWndTextEditView);
		return 0;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case ID_FILE_EXIT:
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			return 0;
		}
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

ATOM InitMainWnd()
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= WndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= GetModuleHandle(0);
	wc.hIcon			= LoadIcon(0, MAKEINTRESOURCE(IDI_WARNING));
	wc.hCursor			= LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground	= reinterpret_cast<HBRUSH>(COLOR_3DFACE+1);
	wc.lpszMenuName		= MAKEINTRESOURCE(IDR_MAINMENU);
	wc.lpszClassName	= g_szAppName;
	wc.hIconSm			= LoadIcon (NULL, IDI_WARNING);

	return RegisterClassEx(&wc);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;

	if (!InitMainWnd()) {
		 MessageBox(NULL, L"Window registration failed!", g_szAppName, MB_ICONEXCLAMATION | MB_OK);
		 return 0;
	}

	hWndMain = CreateWindowEx(0,
				g_szAppName,
				g_szAppName,
				WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				DEFAULT_WIDTH,
				DEFAULT_HEIGHT,
				NULL,
				NULL,
				hInstance,
				NULL);

	if (hWndMain == NULL) {
		MessageBox(NULL, L"Window creation failed!", g_szAppName, MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hWndMain, nShowCmd);
	UpdateWindow(hWndMain);

	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}