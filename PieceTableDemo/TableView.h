#ifndef PIECETABLE_TABLE_VIEW_H
#define PIECETABLE_TABLE_VIEW_H

class PieceTable;

class TableView
{
public:
	TableView(HWND hWnd, PieceTable* pTable);
	~TableView();

	LONG OnPaint();

	LRESULT TableViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam);
	static LRESULT WINAPI TableViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	HWND m_hWnd;
	PieceTable* m_pTable;

	HFONT m_hFont;
};

HWND CreateTableView(HWND hWndParent, PieceTable *pTable);

#endif //PIECETABLE_TABLE_VIEW_H
