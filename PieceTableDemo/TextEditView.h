#ifndef PIECETABLE_TEXT_EDIT_VIEW_H
#define PIECETABLE_TEXT_EDIT_VIEW_H

class PieceTable;

class TextEditView
{
public:
	TextEditView(HWND hWnd, PieceTable* pTable);
	~TextEditView();

	LONG OnPaint();

	LRESULT TextEditViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam);
	static LRESULT WINAPI TextEditViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	void SetFont(HFONT hFont);
	void KeyDown(UINT nKey, UINT nFlags);
	void RepositionCaret(int x, int y);
	bool InputChar(WPARAM wParam, LPARAM lParam);
	bool RemoveChar(WPARAM wParam, LPARAM lParam);

private:
	HWND m_hWnd;
	PieceTable* m_pTable;

	HFONT m_hFont;
	int   m_nLineHeight;

	int   m_nCursorPos;
};

HWND CreateTextEditView(HWND hWndParent, PieceTable *pTable);

#endif //PIECETABLE_TEXT_EDIT_VIEW_H
