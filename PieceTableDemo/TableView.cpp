#include <windows.h>
#include <tchar.h>

#include "TableView.h"
#include "../PieceTable/piecetable.h"

TableView::TableView(HWND hWnd, PieceTable* pTable)
	: m_hWnd(hWnd)
	, m_pTable(pTable)
{
	m_hFont = CreateFont(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _T("Tahoma"));
}

TableView::~TableView()
{
	DeleteObject(m_hFont);
}

void DrawCell(HDC hDC, RECT* pRect, TCHAR* text, size_t length, COLORREF fgColor, COLORREF bgColor, COLORREF borderColor)
{
	HPEN hPen, hOldPen;
	HPEN hBorderPen;
	HBRUSH hBrush, hOldBrush;
	hPen = CreatePen(PS_SOLID, 1, fgColor);
	hBrush = CreateSolidBrush(bgColor);
	hBorderPen = CreatePen(PS_SOLID, 1, borderColor);

	const int MARGIN = 3;

	RECT rect = *pRect;

	// Draw cell background (including border)
	hOldBrush = static_cast<HBRUSH>(SelectObject(hDC, hBrush));
	hOldPen = static_cast<HPEN>(SelectObject(hDC, hBorderPen));
	Rectangle(hDC, rect.left, rect.top, rect.right, rect.bottom);
	SelectObject(hDC, hOldPen);
	SelectObject(hDC, hOldBrush);

	// Draw text
	hOldPen = static_cast<HPEN>(SelectObject(hDC, hPen));
	int nOldBkMode = SetBkMode(hDC, TRANSPARENT);
	TextOut(hDC, rect.left + MARGIN, rect.top + MARGIN, text, length);
	SetBkMode(hDC, nOldBkMode);
	SelectObject(hDC, hOldPen);

	DeleteObject(hPen);
	DeleteObject(hBrush);
	DeleteObject(hBorderPen);
}

LONG TableView::OnPaint()
{
	PAINTSTRUCT ps;
	BeginPaint(m_hWnd, &ps);

	RECT rect;
	GetClientRect(m_hWnd, &rect);

	HDC hdcMem = CreateCompatibleDC(ps.hdc);
	HBITMAP hbmMem = CreateCompatibleBitmap(ps.hdc, rect.right, rect.bottom);
	SelectObject(hdcMem, hbmMem);

	FillRect(hdcMem, &rect, GetSysColorBrush(COLOR_WINDOW));

	HFONT hOldFont = static_cast<HFONT>(SelectObject(hdcMem, m_hFont));

	#define BLACK RGB(0, 0, 0)
	#define WHITE RGB(255, 255, 255)
	#define DEBUG RGB(128, 128, 128)

	const int LEFT = 10;
	const int TOP = 10;
	const int HEIGHT = 40;
	const int OFFSET_COL_WIDTH = 40;
	const int LENGTH_COL_WIDTH = 40;
	const int TEXT_COL_WIDTH = rect.right - rect.left - (2*LEFT) - OFFSET_COL_WIDTH - LENGTH_COL_WIDTH;

	RECT rcStart;
	rcStart.left = LEFT;
	rcStart.top  = TOP;
	rcStart.right = rcStart.left;
	rcStart.bottom = rcStart.top + HEIGHT;

	int tableRow = 0;
	PieceTable::PieceTableIterator it = m_pTable->mPieceTable.begin();
	while (it != m_pTable->mPieceTable.end()) {
		PieceTable::piece* p = &(*it);
		
		TCHAR text[1024+1];

		COLORREF bgColor = WHITE;
		if (tableRow++ % 2 == 0) bgColor = RGB(195, 195, 195);

		// Draw offset in piece
		rcStart.left = rcStart.right;
		rcStart.right = rcStart.left + OFFSET_COL_WIDTH;
		wsprintf(text, L"%u", p->offset);
		DrawCell(hdcMem, &rcStart, text, wcslen(text), BLACK, bgColor, BLACK);

		// Draw length in piece
		rcStart.left = rcStart.right;
		rcStart.right = rcStart.left + LENGTH_COL_WIDTH;
		wsprintf(text, L"%u", p->length);
		DrawCell(hdcMem, &rcStart, text, wcslen(text), BLACK, bgColor, BLACK);

		// Draw text
		rcStart.left = rcStart.right;
		rcStart.right = rcStart.left + TEXT_COL_WIDTH;
		size_t len = min(p->length, 1024);
		wcsncpy_s(text, 1024, m_pTable->getBuffer(p) + p->offset, len);
		text[len] = _T('\0');
		DrawCell(hdcMem, &rcStart, text, len, BLACK, bgColor, BLACK);

		rcStart.top += HEIGHT;
		rcStart.bottom = rcStart.top + HEIGHT;
		rcStart.left = LEFT;
		rcStart.right = rcStart.left;

		it++;
	}

	SelectObject(hdcMem, hOldFont);

	BitBlt(ps.hdc, 0, 0, rect.right, rect.bottom, hdcMem, 0, 0, SRCCOPY);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);

	EndPaint(m_hWnd, &ps);
	return 0;
}

LRESULT TableView::TableViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_ERASEBKGND:
		// Notify we handled the background erase message
		return 1;

	case WM_PAINT:
		return OnPaint();

	default:
		break;
	}

	return DefWindowProc(m_hWnd, msg, wParam, lParam);
}

static LRESULT WINAPI TableViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	TableView *pView = reinterpret_cast<TableView *>(GetWindowLongPtr(hWnd, 0));
	PVOID p;

	switch(msg)
	{
	case WM_NCCREATE:
		p = (reinterpret_cast<CREATESTRUCT *>(lParam))->lpCreateParams;

		if((pView = new TableView(hWnd, static_cast<PieceTable *>(p))) == 0)
			return FALSE;

		SetWindowLongPtr(hWnd, 0, reinterpret_cast<LONG_PTR>(pView));
		return TRUE;

	case WM_NCDESTROY:
		delete pView;
		return 0;

	default:
		return pView->TableViewWndProc(msg, wParam, lParam);
	}
}

const TCHAR CLASS_NAME[] = _T("TableView");

ATOM InitTableView()
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= TableViewWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= sizeof(TableView *);
	wc.hInstance		= GetModuleHandle(0);
	wc.hIcon			= 0;
	wc.hCursor			= LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground	= reinterpret_cast<HBRUSH>(COLOR_WINDOW+1);
	wc.lpszMenuName		= 0;
	wc.lpszClassName	= CLASS_NAME;
	wc.hIconSm			= 0;

	return RegisterClassEx(&wc);
}

HWND CreateTableView(HWND hWndParent, PieceTable *pTable)
{
	if (!InitTableView()) {
		 MessageBox(NULL, L"Window registration failed!", L"Table View", MB_ICONEXCLAMATION | MB_OK);
		 return 0;
	}

	return CreateWindowEx(WS_EX_CLIENTEDGE, CLASS_NAME, 0, WS_VISIBLE|WS_CHILD, 0, 0, 0, 0, hWndParent, 0, 0, static_cast<LPVOID>(pTable));
}