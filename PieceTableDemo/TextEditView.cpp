#include <windows.h>
#include <tchar.h>

#include "TextEditView.h"
#include "../PieceTable/piecetable.h"

TextEditView::TextEditView(HWND hWnd, PieceTable* pTable)
	: m_hWnd(hWnd)
	, m_pTable(pTable)
	, m_nLineHeight(0)
	, m_nCursorPos(0)
{
	m_hFont = CreateFont(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _T("Tahoma"));
	SetFont(m_hFont);
	RepositionCaret(0, 0);
}

TextEditView::~TextEditView()
{
	DeleteObject(m_hFont);
}

LONG TextEditView::OnPaint()
{
	PAINTSTRUCT ps;
	BeginPaint(m_hWnd, &ps);
	
	RECT rect;
	GetClientRect(m_hWnd, &rect);

	HDC hdcMem = CreateCompatibleDC(ps.hdc);
	HBITMAP hbmMem = CreateCompatibleBitmap(ps.hdc, rect.right, rect.bottom);
	SelectObject(hdcMem, hbmMem);

	FillRect(hdcMem, &rect, GetSysColorBrush(COLOR_WINDOW));

	HFONT hOldFont = static_cast<HFONT>(SelectObject(hdcMem, m_hFont));

	HPEN hPen, hOldPen;
	hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));

	wchar_t buf[4096+1];
	m_pTable->render(0, buf, 4096);

	hOldPen = static_cast<HPEN>(SelectObject(hdcMem, hPen));
	int nOldBkMode = SetBkMode(hdcMem, TRANSPARENT);
	TextOut(hdcMem, rect.left, rect.top, buf, wcslen(buf));
	SetBkMode(hdcMem, nOldBkMode);
	SelectObject(hdcMem, hOldPen);

	SelectObject(hdcMem, hOldFont);

	BitBlt(ps.hdc, 0, 0, rect.right, rect.bottom, hdcMem, 0, 0, SRCCOPY);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);

	EndPaint(m_hWnd, &ps);
	return 0;
}

void TextEditView::SetFont(HFONT hFont)
{
	HDC hDC = GetDC(m_hWnd);

	m_hFont = hFont;
	HFONT hOldFont = static_cast<HFONT>(SelectObject(hDC, m_hFont));

	TEXTMETRIC tm;
	GetTextMetrics(hDC, &tm);

	m_nLineHeight = tm.tmHeight + tm.tmExternalLeading;

	SelectObject(hDC, hOldFont);

	ReleaseDC(m_hWnd, hDC);
}

bool IsKeyPressed(UINT uVK)
{
	return (GetKeyState(uVK) & 0x80000000) ? true : false;
}

void TextEditView::RepositionCaret(int x, int y)
{
	wchar_t buf[4096+1];
	m_pTable->render(0, buf, 4096);

	int totalw = 0;
	SIZE sz;
	HDC hDC = GetDC(m_hWnd);
	HFONT hOldFont = static_cast<HFONT>(SelectObject(hDC, m_hFont));
	for (int i = 0; i < x; i++) {	// SLOOWWW!! But allowed for this demo app
		GetTextExtentPoint(hDC, &buf[i], 1, &sz);
		totalw += sz.cx;
	}
	SelectObject(hDC, hOldFont);
	ReleaseDC(m_hWnd, hDC);

	SetCaretPos(totalw, y);
}

bool TextEditView::InputChar(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	WCHAR ch[] = { static_cast<WCHAR>(wParam) };

	if (iswprint(static_cast<wint_t>(wParam))) {
		m_pTable->insert(static_cast<size_t>(m_nCursorPos), const_cast<wchar_t*>(ch), 1);

		InvalidateRect(m_hWnd, 0, FALSE);
		PostMessage(GetParent(m_hWnd), WM_USER, 0, 0);

		return true;
	}

	return false;
}

bool TextEditView::RemoveChar(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	m_pTable->remove(wParam, 1);

	InvalidateRect(m_hWnd, 0, FALSE);
	PostMessage(GetParent(m_hWnd), WM_USER, 0, 0);

	return true;
}

void TextEditView::KeyDown(UINT nKey, UINT nFlags)
{
	UNREFERENCED_PARAMETER(nFlags);

	switch (nKey)
	{
	case VK_CONTROL: case VK_SHIFT:
		return;

	case 'z': case 'Z':
		if(IsKeyPressed(VK_CONTROL))
		{
			// undo
		}
		break;

	case 'y': case 'Y':
		if(IsKeyPressed(VK_CONTROL))
		{
			// redo
		}
		break;

	case VK_BACK:
		if (m_nCursorPos > 0 && RemoveChar(m_nCursorPos - 1, 0)) {
			m_nCursorPos = max(m_nCursorPos - 1, 0);
			RepositionCaret(m_nCursorPos, 0);
		}
		break;

	case VK_DELETE:
		RemoveChar(m_nCursorPos, 0);
		// No need to reposition caret
		break;

	case VK_LEFT:
		m_nCursorPos = max(m_nCursorPos - 1, 0);
		RepositionCaret(m_nCursorPos, 0);
		break;
	
	case VK_RIGHT:
		m_nCursorPos = min(m_nCursorPos + 1, static_cast<int>(m_pTable->size()));
		RepositionCaret(m_nCursorPos, 0);
		break;

	case VK_HOME:
		m_nCursorPos = 0;
		RepositionCaret(m_nCursorPos, 0);
		break;

	case VK_END:
		m_nCursorPos = m_pTable->size();
		RepositionCaret(m_nCursorPos, 0);
		break;
	}
}

LRESULT TextEditView::TextEditViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_SETFONT:
		SetFont((HFONT)wParam);
		return 0;

	case WM_ERASEBKGND:
		// Notify we handled the background erase message
		return 1;

	case WM_PAINT:
		return OnPaint();

	case WM_KILLFOCUS:
		HideCaret(m_hWnd);
		DestroyCaret();
		return 0;

	case WM_SETFOCUS:
		CreateCaret(m_hWnd, NULL, 2, m_nLineHeight);
		ShowCaret(m_hWnd);
		return 0;

	case WM_CHAR:
		if (InputChar(wParam, lParam)) {
			m_nCursorPos = min(m_nCursorPos + 1, static_cast<int>(m_pTable->size()));
			RepositionCaret(m_nCursorPos, 0);
		}
		return 0;

	case WM_KEYDOWN:
		KeyDown(wParam, lParam);
		return 0;
	}

	return DefWindowProc(m_hWnd, msg, wParam, lParam);
}

static LRESULT WINAPI TextEditViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	TextEditView *pView = reinterpret_cast<TextEditView *>(GetWindowLongPtr(hWnd, 0));
	PVOID p;

	switch(msg)
	{
	case WM_NCCREATE:
		p = (reinterpret_cast<CREATESTRUCT *>(lParam))->lpCreateParams;

		if((pView = new TextEditView(hWnd, static_cast<PieceTable *>(p))) == 0)
			return FALSE;

		SetWindowLongPtr(hWnd, 0, reinterpret_cast<LONG_PTR>(pView));
		return TRUE;

	case WM_NCDESTROY:
		delete pView;
		return 0;

	default:
		return pView->TextEditViewWndProc(msg, wParam, lParam);
	}
}

const TCHAR CLASS_NAME[] = _T("TextEditView");

ATOM InitTextEditView()
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= TextEditViewWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= sizeof(TextEditView *);
	wc.hInstance		= GetModuleHandle(0);
	wc.hIcon			= 0;
	wc.hCursor			= LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground	= reinterpret_cast<HBRUSH>((COLOR_WINDOW+1));
	wc.lpszMenuName		= 0;
	wc.lpszClassName	= CLASS_NAME;
	wc.hIconSm			= 0;

	return RegisterClassEx(&wc);
}

HWND CreateTextEditView(HWND hWndParent, PieceTable *pTable)
{
	if (!InitTextEditView()) {
		 MessageBox(NULL, L"Window registration failed!", L"Text Edit View", MB_ICONEXCLAMATION | MB_OK);
		 return 0;
	}

	return CreateWindowEx(WS_EX_CLIENTEDGE, CLASS_NAME, 0, WS_VISIBLE|WS_CHILD, 0, 0, 0, 0, hWndParent, 0, 0, static_cast<LPVOID>(pTable));
}