#ifndef PIECETABLE_GRAPHICAL_VIEW_H
#define PIECETABLE_GRAPHICAL_VIEW_H

class PieceTable;

class GraphicalView
{
public:
	GraphicalView(HWND hWnd, PieceTable* pTable);
	~GraphicalView();

	LONG OnPaint();

	LRESULT GraphicalViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam);
	static LRESULT WINAPI GraphicalViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	HWND m_hWnd;
	PieceTable* m_pTable;

	HFONT m_hFont;
};

HWND CreateGraphicalView(HWND hWndParent, PieceTable *pTable);

#endif //PIECETABLE_GRAPHICAL_VIEW_H
