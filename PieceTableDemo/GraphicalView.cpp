#include <windows.h>
#include <tchar.h>

#include "GraphicalView.h"
#include "../PieceTable/piecetable.h"

GraphicalView::GraphicalView(HWND hWnd, PieceTable* pTable)
	: m_hWnd(hWnd)
	, m_pTable(pTable)
{
	m_hFont = CreateFont(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _T("Tahoma"));
}

GraphicalView::~GraphicalView()
{
	DeleteObject(m_hFont);
}

void DrawNodeBackground(HDC hDC, RECT* pRect, COLORREF fgColor, COLORREF bgColor)
{
	UNREFERENCED_PARAMETER(fgColor);

	bgColor = SetBkColor(hDC, bgColor);
	ExtTextOut(hDC, 0, 0, ETO_OPAQUE, pRect, 0, 0, 0);	
	SetBkColor(hDC, bgColor);
}

void DrawNode(HDC hDC, RECT* pRect, TCHAR* text, size_t length, COLORREF fgColor, COLORREF bgColor, SIZE* pTextSize)
{
	SIZE sz;
	GetTextExtentPoint32(hDC, text, length, &sz);

	RECT rc;
	RECT rect = *pRect;
	SetRect(&rc, rect.left, rect.top, rect.left + sz.cx, rect.top + sz.cy);

	DrawNodeBackground(hDC, &rc, fgColor, bgColor);

	int nOldBkMode = SetBkMode(hDC, TRANSPARENT);
	TextOut(hDC, rc.left, rc.top, text, length);
	SetBkMode(hDC, nOldBkMode);

	if (pTextSize) {
		pTextSize->cx = rc.right - rc.left;
		pTextSize->cy = rc.bottom - rc.top;
	}
}

LONG GraphicalView::OnPaint()
{
	PAINTSTRUCT ps;
	BeginPaint(m_hWnd, &ps);

	RECT rect;
	GetClientRect(m_hWnd, &rect);

	HDC hdcMem = CreateCompatibleDC(ps.hdc);
	HBITMAP hbmMem = CreateCompatibleBitmap(ps.hdc, rect.right, rect.bottom);
	SelectObject(hdcMem, hbmMem);

	FillRect(hdcMem, &rect, GetSysColorBrush(COLOR_WINDOW));

	HFONT hOldFont = static_cast<HFONT>(SelectObject(hdcMem, m_hFont));

	const int OFFSET = 10;
	const int LEFT   = 10;
	const int TOP    = 10;
	const int HEIGHT = 30;

	#define ORIG_COLOR  RGB(0x7F, 0x44, 0xD6)
	#define MOD_COLOR   RGB(0xC9, 0x36, 0xD3)
	#define OTHER_COLOR RGB(0x4D, 0x54, 0xD8)

	RECT rcStart;
	rcStart.left = LEFT;//rcHead.right + OFFSET;
	rcStart.top  = TOP;
	rcStart.right = rcStart.left + 100; /* arbitrary; it is calculated */;
	rcStart.bottom = rcStart.top + HEIGHT;
	PieceTable::PieceTableIterator it = m_pTable->mPieceTable.begin();
	while (it != m_pTable->mPieceTable.end()) {
		PieceTable::piece* p = &(*it);
		SIZE sz;
		DrawNode(hdcMem, &rcStart, m_pTable->getBuffer(p) + p->offset, p->length, OTHER_COLOR, p->type == PieceTable::originalBuffer ? ORIG_COLOR : MOD_COLOR, &sz);
		rcStart.left += sz.cx + OFFSET;
		rcStart.right = rcStart.left;
		it++;
	}

	SelectObject(hdcMem, hOldFont);

	// Draw original buffer
	RECT rcOrgBuffer;
	rcOrgBuffer.left = LEFT;
	rcOrgBuffer.right = rcOrgBuffer.left;
	rcOrgBuffer.top = rcStart.bottom + OFFSET;
	rcOrgBuffer.bottom = rcOrgBuffer.top + 10;
	TextOut(hdcMem, rcOrgBuffer.left, rcOrgBuffer.top, L"Original buffer: ", 17);
	rcOrgBuffer.left += 120;
	SIZE sz;
	if (m_pTable->mOriginalBuffer) {
		DrawNode(hdcMem, &rcOrgBuffer, m_pTable->mOriginalBuffer, wcslen(m_pTable->mOriginalBuffer), OTHER_COLOR, ORIG_COLOR, &sz);
	}

	// Draw modify/append buffer
	RECT rcModBuffer;
	rcModBuffer.left = LEFT;
	rcModBuffer.right = rcModBuffer.left;
	rcModBuffer.top = rcOrgBuffer.bottom + OFFSET;
	rcModBuffer.bottom = rcModBuffer.top + 10;
	TextOut(hdcMem, rcModBuffer.left, rcModBuffer.top, L"Modify buffer: ", 15);
	rcModBuffer.left += 120;
	if (m_pTable->mAppendBuffer) {
		DrawNode(hdcMem, &rcModBuffer, m_pTable->mAppendBuffer, m_pTable->mAppendBufferLength, OTHER_COLOR, MOD_COLOR, &sz);
	}

	BitBlt(ps.hdc, 0, 0, rect.right, rect.bottom, hdcMem, 0, 0, SRCCOPY);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);

	EndPaint(m_hWnd, &ps);
	return 0;
}

LRESULT GraphicalView::GraphicalViewWndProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_ERASEBKGND:
		// Notify we handled the background erase message
		return 1;

	case WM_PAINT:
		return OnPaint();

	default:
		break;
	}

	return DefWindowProc(m_hWnd, msg, wParam, lParam);
}

static LRESULT WINAPI GraphicalViewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	GraphicalView *pView = reinterpret_cast<GraphicalView *>(GetWindowLongPtr(hWnd, 0));
	PVOID p;

	switch(msg)
	{
	case WM_NCCREATE:
		p = (reinterpret_cast<CREATESTRUCT *>(lParam))->lpCreateParams;

		if((pView = new GraphicalView(hWnd, static_cast<PieceTable *>(p))) == 0)
			return FALSE;

		SetWindowLongPtr(hWnd, 0, reinterpret_cast<LONG_PTR>(pView));
		return TRUE;

	case WM_NCDESTROY:
		delete pView;
		return 0;

	default:
		return pView->GraphicalViewWndProc(msg, wParam, lParam);
	}
}

const TCHAR CLASS_NAME[] = _T("GraphView");

ATOM InitGraphicalView()
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= GraphicalViewWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= sizeof(GraphicalView *);
	wc.hInstance		= GetModuleHandle(0);
	wc.hIcon			= 0;
	wc.hCursor			= LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground	= reinterpret_cast<HBRUSH>(COLOR_WINDOW+1);
	wc.lpszMenuName		= 0;
	wc.lpszClassName	= CLASS_NAME;
	wc.hIconSm			= 0;

	return RegisterClassEx(&wc);
}

HWND CreateGraphicalView(HWND hWndParent, PieceTable *pTable)
{
	if (!InitGraphicalView()) {
		 MessageBox(NULL, L"Window registration failed!", L"Graphical View", MB_ICONEXCLAMATION | MB_OK);
		 return 0;
	}

	return CreateWindowEx(WS_EX_CLIENTEDGE, CLASS_NAME, 0, WS_VISIBLE|WS_CHILD, 0, 0, 0, 0, hWndParent, 0, 0, static_cast<LPVOID>(pTable));
}