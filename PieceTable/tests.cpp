#include "piecetable.h"
#include <iostream>
#include <cassert>

using namespace std;

void print(PieceTable* pt)
{
	wchar_t buf[2048 + 1];
	pt->render(0, buf, 2048);
	wcout << buf << endl;
}

void printDebug(PieceTable* pt)
{
	wchar_t buf[2048 + 1];
	pt->renderDebug(buf, 2048);
	wcout << "Debug: " << buf << endl;
}

void check(wchar_t* actual, wchar_t* expected)
{
	int result = wcscmp(actual, expected);
	if (result) {
		wcerr << "Expected <" << expected << ">, Actual <" << actual << ">" << endl;
		//printDebug(pt);
	}
	assert(result == 0);
}

void check(PieceTable* pt, wchar_t* expected)
{
	wchar_t buf[2048 + 1];
	pt->render(0, buf, 2048);
	check(buf, expected);
}

void testInsert()
{
	PieceTable pt;

	// Insert in an empty table
	pt.insert(0, L"Hello World", 11);
	check(&pt, L"Hello World");

	// Insert at a piece boundary (absolute front of table)
	pt.insert(0, L"Hi! ", 4);
	check(&pt, L"Hi! Hello World");

	// Insert in middle of a piece
	pt.insert(10, L"Large ", 6);
	check(&pt, L"Hi! Hello Large World");

	// Insert at a piece boundary (between two pieces)
	pt.insert(4, L"Welcome. ", 9);
	check(&pt, L"Hi! Welcome. Hello Large World");

	// Insert beyond piece table size
	pt.insert(400, L"Test", 4);
	check(&pt, L"Hi! Welcome. Hello Large WorldTest");
}

void testRemoval()
{
	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Removal at piece boundary (absolute front of table)
		pt.remove(0, 6);
		check(&pt, L"World");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Removal at piece boundary (end of piece)
		pt.remove(5, 6);
		check(&pt, L"Hello");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Removal in middle of piece
		pt.remove(9, 1);
		check(&pt, L"Hello Word");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);
		pt.insert(0, L"Hi! ", 4);
		pt.insert(10, L"Large ", 6);
		pt.insert(4, L"Welcome. ", 9);
		// [Start] -> [Hi! ] -> [Welcome. ] -> [Hello ] -> [Large ] -> [World] -> [End]

		// Removal in middle of piece spanning multiple pieces
		pt.remove(15, 13);
		check(&pt, L"Hi! Welcome. Held");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Removal beyond piece table size
		pt.remove(400, 10);
		check(&pt, L"Hello World");
	}

	{
		PieceTable pt;

		// Removal from empty piece table
		pt.remove(0, 10);
		check(&pt, L"");
	}
}

void testReplace()
{
	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Replace at piece boundary (absolute front of table)
		pt.replace(0, 5, L"Developing", 10);
		check(&pt, L"Developing World");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Replace at piece boundary (end of piece)
		pt.replace(6, 5, L"Application", 11);
		check(&pt, L"Hello Application");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Replace at piece boundary (in middle of piece)
		pt.replace(6, 0, L"Large ", 6);
		check(&pt, L"Hello Large World");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Replace in middle of piece
		pt.replace(9, 1, L"rie", 3);
		check(&pt, L"Hello Worried");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);
		pt.insert(0, L"Hi! ", 4); // overwrite
		pt.insert(10, L"Large ", 6);
		pt.insert(4, L"Welcome. ", 9);
		// [Start] -> [Hi! ] -> [Welcome. ] -> [Hello ] -> [Large ] -> [World] -> [End]

		// Replace in middle of piece, spanning multiple pieces
		pt.replace(15, 13, L"llo you are to", 14);
		check(&pt, L"Hi! Welcome. Hello you are told");
	}

	{
		PieceTable pt;
		pt.insert(0, L"Hello World", 11);

		// Replace beyond piece table size
		pt.replace(6, 100, L"You", 3);
		check(&pt, L"Hello You");
	}

	{
		PieceTable pt;

		// Replace from empty piece table
		pt.replace(0, 0, L"Hello", 5);
		check(&pt, L"Hello");
	}

	{
		PieceTable pt;

		// Replace from empty piece table (beyond table size)
		pt.replace(0, 10, L"Hello", 5);
		check(&pt, L"Hello");
	}
}

void testRender()
{
	PieceTable pt;
	pt.insert(0, L"Hello World", 11);
	pt.insert(0, L"Hi! ", 4);
	pt.insert(10, L"Large ", 6);
	pt.insert(4, L"Welcome. ", 9);
	// [Start] -> [Hi! ] -> [Welcome. ] -> [Hello ] -> [Large ] -> [World] -> [End]

	wchar_t buf[2048 + 1];

	pt.render(0, buf, 2048);
	// test size of piece table with rendered data
	size_t size = pt.size();
	size_t buflen = wcslen(buf);
	assert(size == buflen);

	wmemset(buf, 0, 2048+1);
	// render at start of data (not absolute start)
	pt.render(1, buf, 2048);
	check(buf, L"i! Welcome. Hello Large World");

	wmemset(buf, 0, 2048+1);
	// render at end of data (not absolute end)
	pt.render(pt.size()-1, buf, 2048);
	check(buf, L"d");

	wmemset(buf, 0, 2048+1);
	// render at piece boundary (end)
	pt.render(3, buf, 2048);
	check(buf, L" Welcome. Hello Large World");

	wmemset(buf, 0, 2048+1);
	// render at piece boundary (front)
	pt.render(4, buf, 2048);
	check(buf, L"Welcome. Hello Large World");

	wmemset(buf, 0, 2048+1);
	// Render beyond piece table size
	pt.render(400, buf, 2048);
	check(buf, L"");
}

int main(int argc, char* argv[])
{
	testInsert();
	testRemoval();
	testReplace();
	testRender();
	return 0;
}
