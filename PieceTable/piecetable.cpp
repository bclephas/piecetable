// Copyright (c) 2012-2014 Bart Clephas. All rights reserved
//
// No part of this document may be reproduced or transmitted in any form or by any means, electronic, mechanical,
// photocopying, recording, or otherwise, without prior written permission of the author.
//
// DISCLAIMER
// This software is provided 'as is' with no explicit or implied warranties in respect of its properties, including, but
// not limited to, correctness and/or fitness for purpose.

#include "piecetable.h"
#include <cstring>
#include <algorithm>
#include <limits>

using namespace std;

// Optimizations:
// - use a span object instead of inserting/removing the list during operations
// Improvements:
// - replace
// - undo/redo functionality (with the span objects)
// - function documentation

const int BUFFER_ALLOC_SIZE = 4096;

PieceTable::piece::piece(buffertype t, size_w o, size_w l)
	: type(t)
	, offset(o)
	, length(l)
{
}

PieceTable::piece::piece(const PieceTable::piece& rhs)
{
	if (this != &rhs) {
		type   = rhs.type;
		offset = rhs.offset;
		length = rhs.length;
	}
}

PieceTable::piece& PieceTable::piece::operator=(const PieceTable::piece& rhs)
{
	if (this != &rhs) {
		type   = rhs.type;
		offset = rhs.offset;
		length = rhs.length;
	}
	return *this;
}

PieceTable::PieceTable()
	: mOriginalBuffer(NULL)
	, mAppendBuffer(NULL)
	, mAppendBufferLength(0)
	, mMaxAppendBufferLength(0)
	, mPieceTableSize(0)
	, mLastOffset(0)
{
}

PieceTable::~PieceTable()
{
	delete [] mAppendBuffer;
}

size_w PieceTable::size()
{
	return mPieceTableSize;
}

void PieceTable::open(const char_t* text, size_w length)
{
	mPieceTable.clear();
	delete [] mAppendBuffer;
	mAppendBufferLength = 0;

	mOriginalBuffer = const_cast<char_t*>(text);
	mPieceTable.insert(mPieceTable.begin(), piece(originalBuffer, 0, length));
	mPieceTableSize = length;
}

void PieceTable::insert(size_w offset, const char_t* text, size_w length)
{
	size_w appendBufferOffset = 0;
	append(text, length, &appendBufferOffset);

	size_w offsetInSequence = 0;
	PieceTableIterator it = findPiece(offset, &offsetInSequence);

	size_w offsetInPiece = offset - offsetInSequence;

	if (mPieceTable.size() == 0) {
		// insert in a empty piece table
		// [Start] -> [End] becomes
		// [Start] -> [Hello World] -> [End]
		mPieceTable.insert(mPieceTable.begin(), piece(appendBuffer, appendBufferOffset, length));
	} else {
		if (it == mPieceTable.end()) {
			// piece could not be found; ensure we can insert it at the end of the sequence
			offsetInPiece = 0;
		}

		if (offsetInPiece == 0 && offset == mLastOffset) {
			// insert at end of last insert, extend last piece
			// [Start] -> [Hello W] -> [End] becomes
			// [Start] -> [Hello World] -> [End]
			piece* p = &(*--it);
			p->length += length;
		} else if (offsetInPiece == 0) {
			// insert at a piece boundary, create new piece
			// [Start] -> [Hello World] -> [End] becomes
			// [Start] -> [Hi! ] -> [Hello World] -> [End]
			mPieceTable.insert(it, piece(appendBuffer, appendBufferOffset, length));
		} else {
			// insert in middle of a piece, create new pieces and remove old piece
			// [Start] -> [Hi! ] -> [Hello World] -> [End] becomes
			// [Start] -> [Hi! ] -> [Hello ] -> [Large ] -> [World] -> [End]
			piece* p = &(*it);

			mPieceTable.insert(it, piece(p->type, p->offset, offsetInPiece));
			mPieceTable.insert(it, piece(appendBuffer, appendBufferOffset, length));
			mPieceTable.insert(it, piece(p->type, p->offset + offsetInPiece, p->length - offsetInPiece));

			it = mPieceTable.erase(it);
		}
	}

	mLastOffset = offset + length;
	mPieceTableSize += length;
}

void PieceTable::remove(size_w offset, size_w length)
{
	size_w offsetInSequence = 0;
	PieceTableIterator it = findPiece(offset, &offsetInSequence);

	size_w offsetInPiece = offset - offsetInSequence;
	size_w removeLength = length;

	if (it == mPieceTable.end()) {
		return;
	}

	if (offsetInPiece != 0) {
		// remove starts inside a piece, create new piece
		// [Start] -> [Hello World] -> [End] becomes
		// [Start] -> [Hello Wor] -> [d] -> [End]
		piece* p = &(*it);
		mPieceTable.insert(it, piece(p->type, p->offset, offsetInPiece));

		if (offsetInPiece + length < p->length) {
			// we have kept the remove within one piece
			mPieceTable.insert(it, piece(p->type, p->offset + offsetInPiece + length, p->length - offsetInPiece - length));
		}

		removeLength -= min(removeLength, p->length - offsetInPiece);

		// move the iterator further so we are on a valid piece boundary
		mPieceTable.erase(it++);
	}

	// remove pieces within the remove range
	while (removeLength > 0 && it != mPieceTable.end()) {
		piece* p = &(*it);

		if (removeLength < p->length) {
			mPieceTable.insert(it, piece(p->type, p->offset + removeLength, p->length - removeLength));
		}

		removeLength -= min(removeLength, p->length);

		mPieceTable.erase(it++);
	}

	mPieceTableSize -= length;
}

void PieceTable::replace(size_w offset, size_w rm_length, const char_t* text, size_w ins_length)
{
    remove(offset, rm_length);
    insert(offset, text, ins_length);
}

void PieceTable::render(size_w offset, char_t* buffer, size_w bufsize)
{
	render(offset, buffer, bufsize, std::numeric_limits<size_w>::max());
}

void PieceTable::render(size_w offset, char_t* buffer, size_w bufsize, size_w length)
{
	char_t* start = buffer;
	size_w offsetInSequence = 0;
	PieceTableIterator it = findPiece(offset, &offsetInSequence);

	size_w offsetInPiece = offset - offsetInSequence;

	while (it != mPieceTable.end())
	{
		piece* p = &(*it);
		size_w len = min(bufsize - (buffer - start), p->length - offsetInPiece);
		memcpy(buffer, getBuffer(p) + p->offset + offsetInPiece, min(len, length) * sizeof(char_t));
		buffer += len;
		offsetInPiece = 0; // only for the first time

		it++;
	}
	if (bufsize - (buffer - start) > 0) {
		buffer[0] = L'\0';
	}
}

void PieceTable::renderDebug(char_t* buffer, size_w bufsize)
{
	char_t* start = buffer;
	
	if (bufsize < 23) {
		// not enough size
		return;
	}

	memcpy(buffer, L"[Start] -> ", 11);
	buffer += 11;

	PieceTableIterator it = findPiece(0);
	while (it != mPieceTable.end())
	{
		piece* p = &(*it);

		memcpy(buffer, L"[", 1);
		buffer += 1;

		size_w len = min(bufsize - (buffer - start), p->length);
		memcpy(buffer, getBuffer(p) + p->offset, len);
		buffer += len;

		memcpy(buffer, L"] -> ", 5);
		buffer += 5;

		it++;
	}

	memcpy(buffer, L"[End]", 5);
	buffer += 5;

	buffer[0] = L'\0';
}

void PieceTable::append(const char_t* text, size_w length, size_w* offset)
{
	if (mAppendBufferLength + length > mMaxAppendBufferLength) {
		mMaxAppendBufferLength += BUFFER_ALLOC_SIZE;
		char_t* tempBuffer = new char_t[mMaxAppendBufferLength];
		memcpy(tempBuffer, mAppendBuffer, mAppendBufferLength * sizeof(char_t));
		delete [] mAppendBuffer;
		mAppendBuffer = tempBuffer;
	}

	if (offset) {
		*offset = mAppendBufferLength;
	}

	memcpy(mAppendBuffer + mAppendBufferLength, text, length * sizeof(char_t));
	mAppendBufferLength += length;
}

char_t* PieceTable::getBuffer(const piece* p) const
{
	if (!p) {
		return NULL;
	}
	return (p->type == originalBuffer ? mOriginalBuffer : mAppendBuffer);
}

PieceTable::PieceTableIterator PieceTable::findPiece(size_w offset, size_w* offsetInSequence/* = 0*/)
{
	if (offset > mPieceTableSize) {
		return mPieceTable.end();
	}

	size_w currentLength = 0;
	PieceTableIterator it = mPieceTable.begin();
	while (it != mPieceTable.end())
	{
		piece* p = &(*it);
		if (currentLength <= offset && offset < currentLength + p->length) {
			if (offsetInSequence) {
				*offsetInSequence = currentLength;
			}
			return it;
		}

		currentLength += p->length;
		it++;
	}

	return mPieceTable.end();
}
