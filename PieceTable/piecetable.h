// Copyright (c) 2012-2014 Bart Clephas. All rights reserved
//
// No part of this document may be reproduced or transmitted in any form or by any means, electronic, mechanical,
// photocopying, recording, or otherwise, without prior written permission of the author.
//
// DISCLAIMER
// This software is provided 'as is' with no explicit or implied warranties in respect of its properties, including, but
// not limited to, correctness and/or fitness for purpose.

#ifndef _PIECE_TABLE_H
#define _PIECE_TABLE_H

#include <cstddef>
#include <list>

typedef size_t            size_w;
typedef wchar_t           char_t;

/**
 * The piece table performs low-level editing on a document - a sequence of
 * characters, either of narrow or wide (Unicode) type. It does so in a
 * immutable way, the original document is never changed - except on save.
 * The piece table is memory efficient in that the memory needed is a function
 * of edits.
 * When you couple the PieceTable to a memory mapped file, you get almost
 * unlimited file size editing with no performance degradation.
 */
class PieceTable
{
	friend class GraphicalView;
	friend class TableView;

private:
	enum buffertype {
		originalBuffer,     //! Non modifyable buffer containing original contents
		appendBuffer        //! Append-only buffer containing user text
	};

	struct piece
	{
		buffertype type;    //! Which buffer the piece points to
		size_w offset;      //! Offset into the buffer
		size_w length;      //! Length of the piece

		piece(buffertype t, size_w o, size_w l);
		piece(const piece& rhs);
		piece& operator=(const piece& rhs);
	};

	typedef std::list<piece> PieceList;
	typedef PieceList::iterator PieceTableIterator;

private:
	// Prohibit use of copy c'tor and assignment operator
	PieceTable(const PieceTable& rhs);
	PieceTable& operator=(const PieceTable& rhs);

public:
	/**
	 * Constructs a piece table object.
	 */
	explicit PieceTable();

	/**
	 * Destructs this piece table instance.
	 */
	~PieceTable();

	/**
	 * Get the size of the text contained in this piece table instance.
	 * \return the size of the text in the piece table.
	 */
	size_w size();

	/**
	 * Initializes the piece table to only contain the text supplied.
	 * All previous contents of the piece table is lost.
	 * Note: the original buffer must be present at all times when this class is alive. This class
	 * only points to the original buffer.
	 * Suggest to use a memory mapped file which keeps the used memory to a minimum.
	 * \param[in] text - data to insert
	 * \param[in] length - length of \a text
	 */
	void open(const char_t* text, size_w length);

	/**
	 * Inserts length characters from text to the piece table starting at offset.
	 * \param[in] offset - offset to start inserting at
	 * \param[in] text - data to insert
	 * \param[in] length - length of \a text
	 */
	void insert(size_w offset, const char_t* text, size_w length);

	/**
	 * Removes length characters from the piece table starting at offset.
	 * \param[in] offset - offset to start removing at
	 * \param[in] length - number of characters to remove
	 */
	void remove(size_w offset, size_w length);

    /**
     * Replaces rm_length characters from the piece table starting at offset with text
     * \param[in] offset - offset to start replacing at
     * \param[in] rm_length - number of characters to remove
     * \param[in] text - data to insert
     * \param[in] ins_length - lenth of \a text
     */
	void replace(size_w offset, size_w rm_length, const char_t* text, size_w ins_length);

	/**
	 * Renders the contents of the piece table to a user supplied buffer starting at an offset.
	 * \param[in] offset - offset to start rendering at
	 * \param[in] buffer - user supplied buffer (null terminated iff buffer has enough room)
	 * \param[in] bufsize - size of \a buffer
	 */
	void render(size_w offset, char_t* buffer, size_w bufsize);

	/**
	 * Renders the contents of the piece table to a user supplied buffer starting at an offset
	 * and of at most specified length
	 * \param[in] offset - offset to start rendering at
	 * \param[in] buffer - user supplied buffer (null terminated iff buffer has enough room)
	 * \param[in] bufsize - size of \a buffer
	 * \param[in] length - length of the data to render
	 */
	void render(size_w offset, char_t* buffer, size_w bufsize, size_w length);

	/**
	 * Debug only! Renders the contents of the piece table in pieces to be able to debug
	 * the implementation. The representation is like "[Start] -> [Piece] -> [Piece] -> [End]".
	 * \param[in] buffer - pointer to a buffer that holds the data
	 * \param[in] bufsize - size of \a buffer
	 */
	void renderDebug(char_t* buffer, size_w bufsize);

private:
	/**
	 * Append text to the append buffer (ensuring that the append buffer has enough room (in blocksizes of 4096 bytes)).
	 * \param[in] text - data to append
	 * \param[in] length - length of \text
	 * \param[out] offset - the position of insertion in the append buffer in returned in \offset.
	 */
	inline void append(const char_t* text, size_w length, size_w* offset);

	/**
	 * Convenience function to get a pointer to the buffer referred to in a \piece
	 * \param[in] p - pointer to a \piece to get the buffer pointer from
	 * \return a pointer to the actual buffer.
	 */
	/*inline*/ char_t* getBuffer(const piece* p) const;

	/**
	 * Finds the corresponding \piece based on offset and returns the offset within that \piece if available.
	 * If the piece is not found, the value of offsetInPiece is undefined.
	 * \param[in] offset - offset to seek
	 * \param[out] offsetInPiece - containing the offset within the found piece
	 * \return a \PieceTableIterator pointing to the piece or end() if none found.
	 */
	inline PieceTableIterator findPiece(size_w offset, size_w* offsetInPiece = 0);

private:
	char_t*   mOriginalBuffer;        //! Pointer to the original data
	char_t*   mAppendBuffer;          //! Pointer to a buffer containing inserted data
	size_w    mAppendBufferLength;    //! Length of the append buffer
	size_w    mMaxAppendBufferLength; //! Maximum allowed length of the append buffer

	PieceList mPieceTable;            //! Piece table instance
	size_w    mPieceTableSize;        //! Size of the piece table

	size_w    mLastOffset;            //! Offset of last edit, to increase performance
};

#endif //_PIECE_TABLE_H
