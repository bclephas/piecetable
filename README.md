PieceTable demonstration
========================
Copyright (C) 2012, Bart Clephas <bclephas@gmail.com>

This project is a demonstration of a piece table implementation in pure C++.
Test projects in pure Win32 and Qt are provided to test the implementation and to see its behavior.
Note the Win32 application was made before Qt and I didn't want to remove it - we can learn from it too.

The project has 3 components:
* PieceTable - implementation
* PieceTableDemo - Win32 demo and test application
* PieceTableDemoQt - Qt demo and test application

Build instructions
------------------
### Win32

Just run the PieceTable.sln file. (You need Visual Studio 2010 for it, although it shouldn't be too
hard to use it with Visual Studio 2008 or Visual Studio 2005 - there's nothing specific for VS2010).

### Linux/Mac

Go to the PieceTableDemoQt folder and enter the following commands:

    qmake -project
    qmake
    make

When finished, run the PieceTableDemoQt executable.

Inclusion in your own project
-----------------------------
Just copy the piecetable.cpp and piecetable.h files in your project. Someday I might make an actual library out of it.
